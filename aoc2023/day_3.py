from dotenv import load_dotenv
from aocd.models import Puzzle
import re

load_dotenv()

number_regex = re.compile("\\d+")


def is_adjacent_to(target_num: tuple[int, re.Match], x_idx: int, y_idx: int) -> bool:
    target_x, match = target_num

    if abs(x_idx - target_x) > 1:
        return False
    start, end = match.span()
    if abs(y_idx - start) > 1 and abs(y_idx - end) > 1:
        return False
    return True


# double array for grid, scan for symbols, sum
def part1(input: str):
    grid = [[ch for ch in line] for line in input.splitlines()]

    all_numbers = set()
    for x_idx, line in enumerate(input.splitlines()):
        for match in number_regex.finditer(line):
            all_numbers.add((x_idx, match))

    numbers = set()

    for x_idx, x_val in enumerate(grid):
        for y_idx, y_val in enumerate(x_val):
            if y_val == "." or number_regex.match(y_val):
                continue
            else:
                for num in all_numbers:
                    if is_adjacent_to(num, x_idx, y_idx):
                        numbers.add(num)

    converted_numbers = [int(num.group(0)) for (_, num) in numbers]
    return sum(converted_numbers)


def part2(input: str):
    pass


def run():
    puz = Puzzle(year=2023, day=3)

    print(part1(puz.input_data))
    # puz.answer_a = part1(puz.input_data)
    # puz.answer_b = part2(puz.input_data)
    #
    # print(puz.answers)


if __name__ == "__main__":
    run()
