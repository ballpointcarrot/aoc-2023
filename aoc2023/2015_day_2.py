from dotenv import load_dotenv
from aocd.models import Puzzle

load_dotenv()


def sqft(present: str) -> int:
    [l, w, h] = [int(dim) for dim in present.split("x")]
    areas = [2 * l * w, 2 * w * h, 2 * h * l]
    areas.sort()
    return sum(areas) + int((areas[0] / 2))


def ribbon(present: str) -> int:
    dims = [int(dim) for dim in present.split("x")]
    dims.sort()
    [l, w, h] = dims
    perimeter = 2 * dims[0] + 2 * dims[1]
    return perimeter + (l * w * h)


def part1(input_data: str) -> int:
    presents = input_data.splitlines()
    sqfts = [sqft(p) for p in presents]
    return sum(sqfts)


def part2(input_data: str) -> int:
    presents = input_data.splitlines()
    ribbons = [ribbon(p) for p in presents]

    return sum(ribbons)


def run():
    puz = Puzzle(year=2015, day=2)

    print(part1(puz.input_data))
    print(part1("2x3x4\n1x1x10"))
    print(part2(puz.input_data))
    print(part2("2x3x4\n1x1x10"))
    puz.answer_a = part1(puz.input_data)
    puz.answer_b = part2(puz.input_data)


run()
