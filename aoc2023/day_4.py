from dotenv import load_dotenv
from aocd.models import Puzzle
import copy
import re

load_dotenv()

card_regex = re.compile(
    "Card\\s+(?P<card_id>\\d+)"
)  # ":\\s+(?P<winning_numbers>\\d+\\s+)+\\|\\s+(?P<played_numbers>\\d+\\s*)+$")


class Card:
    def __init__(
        self, card_id: int | None, winning_numbers: set[int], played_numbers: set[int]
    ):
        self.card_id = card_id
        self.winning_numbers = winning_numbers
        self.played_numbers = played_numbers


def coalesce(match: re.Match[str] | None, key: str):
    if match is None:
        return None
    else:
        return int(match[key])


def parse_ticket(input: str):
    cards = []
    for line in input.splitlines():
        card, numbers = line.split(":")
        winning_numbers, played_numbers = numbers.split("|")
        card = Card(
            coalesce(card_regex.match(card), "card_id"),
            set([int(num) for num in re.findall("\\d+", winning_numbers)]),
            set([int(num) for num in re.findall("\\d+", played_numbers)]),
        )
        cards.append(card)

    return cards


def card_points(card: Card) -> int:
    matched_numbers = card.winning_numbers.intersection(card.played_numbers)
    count = len(matched_numbers)
    points = 2 ** (count - 1) if count > 0 else 0
    return points


def part1(input: str):
    cards = parse_ticket(input)
    points = [card_points(card) for card in cards]
    return sum(points)


def create_card_groups(cards: list[Card]):
    card_groups = {}
    for card in cards:
        card_groups[card.card_id] = [card]
    return card_groups


def part2(input: str):
    cards = parse_ticket(input)
    card_groups = create_card_groups(cards)

    for id, cards in card_groups.items():
        for card in cards:
            matched_count = len(card.winning_numbers.intersection(card.played_numbers))
            for i in range(matched_count):
                idx = id + i + 1
                card_groups[idx].append(copy.copy(card_groups[idx][0]))

    total = 0
    for id, cards in card_groups.items():
        print("Card {}: {}".format(id, len(cards)))
        total += len(cards)

    return total


def run():
    puz = Puzzle(year=2023, day=4)

    puz.answer_a = part1(puz.input_data)
    puz.answer_b = part2(puz.input_data)

    print(puz.answers)


if __name__ == "__main__":
    run()
