from dotenv import load_dotenv
from aocd.models import Puzzle

load_dotenv()


def part1(puz: Puzzle) -> int:
    pos = 0
    for chr in puz.input_data:
        if chr == "(":
            pos = pos + 1
        elif chr == ")":
            pos = pos - 1

    return pos


def part2(puz: Puzzle) -> int:
    pos = 0
    for idx, chr in enumerate(puz.input_data):
        if chr == "(":
            pos = pos + 1
        elif chr == ")":
            pos = pos - 1
        if pos < 0:
            return idx + 1
    return -1


def run():
    day1 = Puzzle(year=2015, day=1)

    print(part1(day1))
    print(part2(day1))
    day1.answer_a = part1(day1)
    day1.answer_b = part2(day1)

    print(day1.answers)


run()
