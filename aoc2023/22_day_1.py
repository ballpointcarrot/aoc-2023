from dotenv import load_dotenv
from aocd.models import Puzzle

load_dotenv()


def get_elves(input):
    return input.split("\n\n")


def get_total_elf_calories(elf):
    entries = elf.splitlines()
    total = 0
    for e in entries:
        cals = int(e)
        total = total + cals

    return total


def run():
    day1 = Puzzle(year=2022, day=1)
    elf_totals = [get_total_elf_calories(elf) for elf in get_elves(day1.input_data)]

    elf_totals.sort()

    day1.answer_a = elf_totals[-1]
    day1.answer_b = sum(elf_totals[-3:])

    print(day1.answers)


run()
