from dotenv import load_dotenv
from aocd.models import Puzzle
import re

load_dotenv()

game_regex = re.compile("^Game (?P<id>\\d+):")
cubes_regex = re.compile("(?P<count>\\d+) (?P<color>\\w+)")


def get_cube_counts(line: str):
    return list(cubes_regex.finditer(line))


def part1(input: str):
    cubes = {"red": 12, "green": 13, "blue": 14}

    valid_ids = []
    for line in input.splitlines():
        invalid = False
        game_id = 0
        matcher = game_regex.match(line)
        if isinstance(matcher, re.Match):
            game_id = int(matcher.group("id"))
        for match in get_cube_counts(line):
            color = match["color"]
            count = int(match["count"])
            if cubes[color] < count:
                invalid = True
        if not invalid:
            valid_ids.append(game_id)
    return sum(valid_ids)


def convert_matches(match_set: list[re.Match[str]]) -> dict[str, int]:
    cubes = {}
    for match in match_set:
        cubes[match["color"]] = int(match["count"])

    return cubes


def part2(input: str):
    powers = []
    for line in input.splitlines():
        pull_splits = line.split(";")
        pull_matches = [get_cube_counts(split) for split in pull_splits]
        max_cubes = {"red": 0, "green": 0, "blue": 0}

        for match_set in pull_matches:
            cube_counts = convert_matches(match_set)
            for color, count in cube_counts.items():
                if max_cubes[color] < count:
                    max_cubes[color] = count

        powers.append(max_cubes["red"] * max_cubes["green"] * max_cubes["blue"])

    return sum(powers)


def run():
    puz = Puzzle(year=2023, day=2)

    puz.answer_a = part1(puz.input_data)
    puz.answer_b = part2(puz.input_data)

    print(puz.answers)


if __name__ == "__main__":
    run()
