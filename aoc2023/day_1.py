from dotenv import load_dotenv
from aocd.models import Puzzle
import re

load_dotenv()

number_regex = re.compile("\\d")
written_numbers = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]


def match_numerals(line: str):
    """Finds all matches against a digit regex."""
    return list(number_regex.finditer(line))


def build_number_finditer(line: str):
    matches = match_numerals(line)
    min_match = min(matches, key=lambda match: match.start())
    max_match = max(matches, key=lambda match: match.start())

    return int(min_match.group(0) + max_match.group(0))


def char_from_word(word: str):
    """Collects a string numeral from a matching word
    corresponding to that numeral."""
    if word in written_numbers:
        idx = written_numbers.index(word)
        return str(idx + 1)
    else:
        return word


def get_matches(line: str):
    search_regexes = [re.compile(digit) for digit in written_numbers]

    # build a list of matches from all the disparate regexes used.
    matches = []
    for r in search_regexes:
        for match in r.finditer(line):
            matches.append(match)

    for match in match_numerals(line):
        matches.append(match)

    # use the starting match index to determine min/max
    min_match = min(matches, key=lambda match: match.start())
    max_match = max(matches, key=lambda match: match.start())

    min_str = char_from_word(min_match[0]) if len(min_match[0]) > 1 else min_match[0]
    max_str = char_from_word(max_match[0]) if len(max_match[0]) > 1 else max_match[0]

    return int(min_str + max_str)


def part1(input: str) -> int:
    numbers = [build_number_finditer(line) for line in input.splitlines()]
    return sum(numbers)


def part2(input: str):
    numbers = [get_matches(line) for line in input.splitlines()]
    return sum(numbers)


def run():
    day1 = Puzzle(year=2023, day=1)

    # Do any logic needed to get answers here

    day1.answer_a = part1(day1.input_data)
    day1.answer_b = part2(day1.input_data)

    print(day1.answers)


# Test code below


def test_build_number():
    print(build_number_finditer("1abc2"))
    assert build_number_finditer("1abc2") == 12


if __name__ == "__main__":
    run()
