from aoc2023 import day_1


def test_part_1():
    example = """1abc2
    pqr3stu8vwx
    a1b2c3d4e5f
    treb7uchet"""
    result: int = day_1.part1(example)
    print(result)
    assert result == 142


def test_part_2():
    example = """two1nine
    eightwothree
    abcone2threexyz
    xtwone3four
    4nineeightseven2
    zoneight234
    7pqrstsixteen"""
    result: int = day_1.part2(example)
    print(result)
    assert result == 281
