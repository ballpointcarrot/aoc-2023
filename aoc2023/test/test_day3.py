from aoc2023 import day_3

example = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""


def test_part1():
    result = day_3.part1(example)
    assert result == 4361


def test_part2():
    assert 1 == 2
