from aoc2023 import day_2

example = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"""


def test_part1():
    result = day_2.part1(example)
    assert result == 8


def test_get_cube_counts():
    cube_counts = day_2.get_cube_counts(
        "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
    )
    print(cube_counts)

    assert len(cube_counts) == 6

    assert cube_counts[0]["count"] == "3"
    assert cube_counts[0]["color"] == "blue"


def test_convert_matches():
    cube_counts = day_2.get_cube_counts("Game 1: 3 blue, 4 red")
    cubes = day_2.convert_matches(cube_counts)

    assert cubes["blue"] == 3
    assert cubes["red"] == 4

    cube_counts = day_2.get_cube_counts("; 1 red, 2 green, 6 blue")
    cubes = day_2.convert_matches(cube_counts)
    assert cubes["red"] == 1
    assert cubes["green"] == 2
    assert cubes["blue"] == 6

    cube_counts = day_2.get_cube_counts("; 2 green")
    cubes = day_2.convert_matches(cube_counts)
    assert cubes["green"] == 2


def test_part2():
    result = day_2.part2(example)

    assert result == 2286
