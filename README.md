# aoc-2023

My set of solutions for the 2023 [Advent of Code](https://adventofcode.com) problem set.

## Installation

No installation needed; problems are run as part of the source bundle.

## Usage

```bash
python run.py
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
